﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    class RentalVehicle
    {
        public IVehicle Vehicle { get; private set; }

        public bool IsFree { get; set; }

        public Client Client { get; set; }

        public DateTime? RentDateTime { get; set; }

        public RentalVehicle(IVehicle vehicle)
        {
            Vehicle = vehicle;
            IsFree = true;
        }

        public bool CanBeTaken(Type type)
        {
            return IsFree && GetType() == type;
        }

        public void ShowInfo()
        {
            string vehicleType = string.Empty;
            if (Vehicle is Car)
            {
                vehicleType = "Car";
            }
            else if (Vehicle is Scooter)
            {
                vehicleType = "Scooter";
            }
            else
            {
                vehicleType = "unknown";
            }

            string msg = $"Type: {vehicleType} | Plate: {Vehicle.Plate} | Brand: {Vehicle.Brand} | Model: {Vehicle.Model}";

            Console.WriteLine(msg);
        }
    }
}
