﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace CarRental
{
    public class CarRepo : Repo
    {
        public int Create(Car newCar)
        {
            string sqlQuery = string.Format(@"Insert into Cars (Brand, Model ,Plate, Seats, IsSeatBelts, CreatedDate, CreatedBy) 
                Values('{0}', '{1}', '{2}', {3}, {4}, {5}, '{6}'); Select @@Identity",
                newCar.Brand,
                newCar.Model,
                newCar.Plate,
                newCar.Seats,
                newCar.IsSeatBelts ? 1 : 0,
                DateTime.Now.ToString("yyyy-MM-dd"),
                "Szymon");

            //Create and open a connection to SQL Server 
            SqlConnection connection = new SqlConnection("Server=mssql2.webio.pl,2401;Database=kxson_training;User ID=kxson_training;Password=ha$loDoBa2y2017$");
            connection.Open();

            //Create a Command object
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            //Execute the command to SQL Server and return the newly created ID
            int newCarId = Convert.ToInt32((decimal)command.ExecuteScalar());

            //Close and dispose
            command.Dispose();
            connection.Close();
            connection.Dispose();

            // Set return value
            return newCarId;
        }

        public List<Car> Return()
        {
            var cars = new List<Car>();
            string queryString = @"SELECT * FROM cars ";

            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (var command = new SqlCommand(queryString, connection))
                    {
                        using (DbDataReader rdr = command.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                var idColumnName = "Id";
                                var id = (int)rdr[idColumnName];
                                var brand = (string)rdr["Brand"];
                                var model = (string)rdr["Model"];
                                var plate = (string)rdr["Plate"];
                                var seats = (int)rdr["Seats"];
                                var isSeatBelts = (bool)rdr["IsSeatBelts"];
                                var createdDate = (DateTime)rdr["CreatedDate"];
                                var createdBy = (string)rdr["CreatedBy"];

                                var carFromDb = new Car(seats, brand, model, plate);
                                carFromDb.Id = id;
                                carFromDb.IsSeatBelts = isSeatBelts;

                                cars.Add(carFromDb);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return cars;
        }

        public void Update(Car car)
        {
            var sqlUpdateQuery = @"Update cars set plate = @pPlate where id = @pId";

            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(sqlUpdateQuery, connection);
                var plateParam = new SqlParameter("pPlate", System.Data.SqlDbType.Text);
                plateParam.Value = car.Plate;
                command.Parameters.Add(plateParam);

                command.Parameters.Add(new SqlParameter("pId", System.Data.SqlDbType.Int) { Value = car.Id });

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void Delete(int carId)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["TrainingConnection"].ConnectionString;
            var sqlUpdateQuery = @"delete cars where id = @pId";

            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(sqlUpdateQuery, connection);
                command.Parameters.Add(new SqlParameter("pId", System.Data.SqlDbType.Int) { Value = carId });
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
