﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    class Scooter : Vehicle
    {
        public Scooter(string brand, string model, string plate) : base(2, brand, model, plate)
        {

        }

        public override void Run()
        {
            Console.WriteLine("pi pi");
        }
    }
}
