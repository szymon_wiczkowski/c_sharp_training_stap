﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    public class Client
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool IsDriver { get; private set; }

        public DateTime DateOfBirth { get; set; }

        public int Age
        {
            get
            {
                return DateTime.Now.Year - DateOfBirth.Year;
            }
        }

        public Client(string firstName, string lastName, bool isDriver, DateTime dateOfBirth)
        {
            FirstName = firstName;
            LastName = lastName;
            IsDriver = isDriver;
            DateOfBirth = dateOfBirth;
        }
    }
}
