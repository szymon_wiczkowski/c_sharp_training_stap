﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dateOfBirthJacek = new DateTime(1986, 2, 14);
            dateOfBirthJacek = dateOfBirthJacek.AddDays(-5);

            Client kowalski = new Client("Jan", "Kowalski", true, dateOfBirthJacek);

            Scooter maly1 = new Scooter("tata", "india", "GD 3567");
            Scooter maly2 = new Scooter("honda", "kj40", "GD 3421");
            Car passat = new Car(5, "VW", "Passat", "GD 3322");
            Car golf = new Car(5, "VW", "Golf", "GD 3564");
            Car yaris = new Car(4, "Toyota", "Yaris", "GD 3214");

            CarRental carRental = new CarRental();
            carRental.AddVehicleToList(maly1);
            carRental.AddVehicleToList(maly2);
            carRental.AddVehicleToList(passat);
            carRental.AddVehicleToList(yaris);
            carRental.AddVehicleToList(golf);

            carRental.ShowAllFreeVehicles();

            var takenCar = carRental.TakeCar(kowalski);
            Car takenCar2 = carRental.TakeCar(kowalski);

            carRental.ShowAllFreeVehicles();

            RentalVehicle info = carRental.GetVehicleWithPlateLinq("GD 3421");
            info.ShowInfo();

            RentalVehicle info2 = carRental.GetVehicleWithPlateLinq("ble");
            if (info2 != null)
                info2.ShowInfo();

            var vehiclesWithSameBrand = carRental.GetVehicleByBrandLinq("VW");
            Console.WriteLine("Vehicles with the same brand");
            foreach (var vehicle in vehiclesWithSameBrand)
            {
                vehicle.ShowInfo();
            }

            Console.WriteLine("--> result 1");
            carRental.GetAndShow("VW", "", null);

            Console.WriteLine("--> result 2");
            carRental.GetAndShow("", "GD 7634", null);

            Console.WriteLine("--> result 3");
            carRental.GetAndShow("", "", 5);

            Console.WriteLine("--> result 4");
            carRental.GetAndShow("Fiat", "", 5);

            var carRepo = new CarRepo();
            //carRepo.Delete(6);

            var newCar = new Car(5, "Fiat", "Panda", "GD 6666")
            {
                Id = 6
            };
            //var newCarId = carRepo.Create(newCar);
            //Console.WriteLine($"New car is added Id:{newCarId}, Model: {newCar.Model}, Brand: {newCar.Brand}, Plate: {newCar.Plate}");

            //panda.Plate = "la la la";
            //carRepo.Update(newCar);

            var cars = carRepo.Return();

            cars.ForEach(car =>
            {
                Console.WriteLine($"Brand: {car.Brand} | Model: {car.Model} | Plate: {car.Plate} | Seats: {car.Seats} | IsSeatBelts: {car.IsSeatBelts}");
                //add created by
            });

            var clientRepo = new ClientRepo();
            //clientRepo.Update(1, "Luck", string.Empty);
            var xxx = clientRepo.Return();
            Console.ReadKey();
        }
    }
}
