﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRental.FolderX;

namespace CarRental
{
    public class ClientRepo : Repo
    {
        public int Create(Client newClient)
        {
            string sqlQuery = string.Format(@"Insert into Clients (FirstName, LastName, IsDriver, DateOfBirth, CreatedDate, CreatedBy) 
                Values(@pFirstName, @pLastName, @pIsDriver, @pDateOfBirth, @pCreatedDate, @pCreatedBy); Select @@Identity");

            //Create and open a connection to SQL Server 
            using (SqlConnection connection = new SqlConnection("Server=mssql2.webio.pl,2401;Database=kxson_training;User ID=kxson_training;Password=ha$loDoBa2y2017$"))
            {
                connection.Open();

                //Create a Command object
                SqlCommand command = new SqlCommand(sqlQuery, connection);
                command.Parameters.Add(new SqlParameter("pFirstName", SqlDbType.Text) { Value = newClient.FirstName });
                command.Parameters.Add(new SqlParameter("pLastName", SqlDbType.Text) { Value = newClient.LastName });
                command.Parameters.Add(new SqlParameter("pIsDriver", SqlDbType.Bit) { Value = newClient.IsDriver });
                command.Parameters.Add(new SqlParameter("pDateOfBirth", SqlDbType.DateTime) { Value = newClient.DateOfBirth });
                command.Parameters.Add(new SqlParameter("pCreatedDate", SqlDbType.DateTime) { Value = DateTime.Now });
                command.Parameters.Add(new SqlParameter("pCreatedBy", SqlDbType.Text) { Value = "Szymon" });

                //Execute the command to SQL Server and return the newly created ID
                return Convert.ToInt32((decimal)command.ExecuteScalar());
            }
        }

        public List<Client> Return()
        {
            var clients = new List<Client>();
            string queryString = @"SELECT * FROM clients ";

            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (var command = new SqlCommand(queryString, connection))
                    {
                        using (DbDataReader rdr = command.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                var id = (int)rdr["Id"];
                                var firstName = (string)rdr["FirstName"];
                                var lastName = (string)rdr["LastName"];
                                var isDriver = (bool)rdr["IsDriver"];
                                var dateOfBirth = (DateTime)rdr["DateOfBirth"];
                                var createdDate = (DateTime)rdr["CreatedDate"];
                                var createdBy = (string)rdr["CreatedBy"];

                                var carFromDb = new Client(firstName, lastName, isDriver, dateOfBirth);
                                carFromDb.Id = id;

                                clients.Add(carFromDb);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return clients;
        }

        public void Update(int id, string firstName, string lastName)
        {
            var sqlUpdateQuery = @"update clients set ";

            if (!string.IsNullOrEmpty(firstName))
            {
                sqlUpdateQuery += @" firstName = @pFirstName,";
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                sqlUpdateQuery += @" lastName = @pLastName,";
            }

            sqlUpdateQuery = sqlUpdateQuery.Remove(sqlUpdateQuery.Length - 1);

            sqlUpdateQuery += @" where id = @pId";

            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(sqlUpdateQuery, connection);

                if (!string.IsNullOrEmpty(firstName))
                    command.Parameters.Add(new SqlParameter("pFirstName", System.Data.SqlDbType.Text) { Value = firstName });

                if (!string.IsNullOrEmpty(lastName))
                    command.Parameters.Add(new SqlParameter("pLastName", System.Data.SqlDbType.Text) { Value = lastName });

                command.Parameters.Add(new SqlParameter("pId", System.Data.SqlDbType.Int) { Value = id });

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void Delete(int id)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["TrainingConnection"].ConnectionString;
            var sqlUpdateQuery = @"delete clients where id = @pId";

            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(sqlUpdateQuery, connection);
                command.Parameters.Add(new SqlParameter("pId", System.Data.SqlDbType.Int) { Value = id });
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
