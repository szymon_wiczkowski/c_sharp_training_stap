﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    class CarRental
    {
        List<RentalVehicle> RentalVehicles { get; set; }

        public CarRental()
        {
            RentalVehicles = new List<RentalVehicle>();
        }

        public void AddVehicleToList(IVehicle vehicle)
        {
            RentalVehicle rentalVehicle = new RentalVehicle(vehicle);
            RentalVehicles.Add(rentalVehicle);
        }

        public Car TakeCar(Client client)
        {
            if (!client.IsDriver)
            {
                Console.WriteLine("client is not a driver");
                return null;
            }

            foreach (RentalVehicle vehicle in RentalVehicles)
            {
                /*
                if(!Vehicle.IsFree)
                {
                    continue;
                }

                //if (vehicle.Vehicle.GetType() != typeof(Car))
                if (!(vehicle.Vehicle is Car))
                {
                    continue;
                }
                */

                if (vehicle.CanBeTaken(typeof(Car)))
                {
                    vehicle.IsFree = false;
                    vehicle.Client = client;
                    vehicle.RentDateTime = DateTime.Now;

                    return (Car)vehicle.Vehicle;
                }

            }

            return null;
        }

        public Scooter TakeScooter(Client client)
        {
            if (client.Age < 18)
            {
                Console.WriteLine("client is too young");
                return null;
            }

            foreach (RentalVehicle vehicle in RentalVehicles)
            {
                if (vehicle.CanBeTaken(typeof(Scooter)))
                {
                    vehicle.IsFree = false;
                    vehicle.Client = client;
                    vehicle.RentDateTime = DateTime.Now;

                    return (Scooter)vehicle.Vehicle;
                }
            }

            return null;
        }

        public void ShowAllFreeVehicles()
        {
            Console.WriteLine("Car Rental Status");

            foreach (var vehicle in RentalVehicles)
            {
                if (!vehicle.IsFree)
                    continue;

                vehicle.ShowInfo();
            }

            Console.WriteLine("----------------------");
        }

        public RentalVehicle GetVehicleWithPlate(string plate)
        {
            foreach (RentalVehicle vehicle in RentalVehicles)
            {
                if (vehicle.Vehicle.Plate == plate)
                {
                    return vehicle;
                }
            }

            Console.WriteLine($"Can't find vehicle wuth plate: {plate}");
            return null;
        }

        public List<RentalVehicle> GetVehicleByBrand(string brand)
        {
            var list = new List<RentalVehicle>();

            foreach (RentalVehicle vehicle in RentalVehicles)
            {
                if (vehicle.Vehicle.Brand == brand)
                {
                    list.Add(vehicle);
                }
            }

            return list;
        }

        //public List<RentalVehicle> Get(string brand, string plate, int? seats)
        //{
        //    IEnumerable<RentalVehicle> listaWynikowa;

        //    if (!string.IsNullOrEmpty(brand))
        //        listaWynikowa = RentalVehicles.Where(veh => veh.Vehicle.Brand == brand);

        //    if (!string.IsNullOrEmpty(plate))
        //        listaWynikowa = RentalVehicles.Where(veh => veh.Vehicle.Plate == plate);

        //    if(seats.HasValue)
        //        listaWynikowa = RentalVehicles.Where(veh => veh.Vehicle.Seats == seats);

        //    return listaWynikowa.ToList();
        //}

        public List<RentalVehicle> GetVehicleByBrandLinq(string brand)
        {
            var listaWynikowa = RentalVehicles.Where(veh => veh.Vehicle.Brand == brand);

            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
            {
                listaWynikowa = listaWynikowa.Where(l => l.IsFree);
            }

            return listaWynikowa.ToList();
        }

        public RentalVehicle GetVehicleWithPlateLinq(string plate)
        {
            return RentalVehicles.SingleOrDefault(r => r.Vehicle.Plate == plate);
        }

        public List<RentalVehicle> Get(string brand, string plate, int? seats)
        {
            return RentalVehicles.Where(r =>
                (brand == "" || r.Vehicle.Brand == brand) &&
                (plate == "" || r.Vehicle.Plate == plate) &&
                (!seats.HasValue || r.Vehicle.Seats == seats)
            ).ToList();
        }

        public void GetAndShow(string brand, string plate, int? seats)
        {
            var result = Get(brand, plate, seats);
            ShowGroupInfo(result);
        }

        private void ShowGroupInfo(List<RentalVehicle> group)
        {
            foreach (var vehicle in group)
            {
                vehicle.ShowInfo();
            }
        }
    }
}
