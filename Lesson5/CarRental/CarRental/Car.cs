﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    public class Car : Vehicle
    {
        public bool IsSeatBelts { get; set; }

        public Car(int seats, string brand, string model, string plate) : base(seats, brand, model, plate)
        {
            IsSeatBelts = true;
        }
    }
}
