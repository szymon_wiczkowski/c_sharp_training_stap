﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    interface IVehicle
    {
        int Seats { get; set; }

        string Plate { get; set; }

        string Brand { get; set; }

        string Model { get; set; }

        void Run();
    }
}
