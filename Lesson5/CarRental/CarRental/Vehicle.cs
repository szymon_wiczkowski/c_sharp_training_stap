﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental
{
    public class Vehicle : IVehicle
    {
        public int Id { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public string Plate { get; set; }

        public int Seats { get; set; }

        public Vehicle(int seats, string brand, string model, string plate)
        {
            Seats = seats;
            Brand = brand;
            Model = model;
            Plate = plate;
        }

        public virtual void Run()
        {
            Console.WriteLine("brum brum");
        }
    }
}
