﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Losator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--Losator--");
            Console.Write("Max number: ");
            var maxNumber = Console.ReadLine();
            var number = new Random().Next(1, int.Parse(maxNumber));
            var message = string.Format("Happy number is: {0}", number);
            Console.WriteLine(message);
            Console.ReadKey();
        }
    }
}
