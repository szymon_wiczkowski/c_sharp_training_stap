﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2_switch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Options: ");
            Console.WriteLine("1 Show temperature");
            Console.WriteLine("2 Show time");
            Console.WriteLine("3 Show location");

            string selectedOption = Console.ReadLine();

            switch (selectedOption)
            {
                case "1":
                    Console.WriteLine("Temperature: 23'C");
                    break;
                case "2":
                    Console.WriteLine("Time: " + DateTime.Now);
                    break;
                case "3":
                    Console.WriteLine("Location: Gdansk");
                    break;
                default:
                    Console.WriteLine("Wrong selection");
                    break;
            }

            Console.ReadKey();
        }
    }
}
