﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2_for
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("for: ");

            var names = new[] { "Luck", "Vader", "Yoda", "Han", "Leya" };

            for (int i = 0; i < names.Length; i++)
            {
                Console.WriteLine("i: " + i);
                Console.WriteLine("Name is: " + names[i]);
            }

            Console.ReadKey();
        }
    }
}
