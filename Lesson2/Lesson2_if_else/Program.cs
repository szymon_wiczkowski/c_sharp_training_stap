﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2_if_else
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("even/odd number");

            Console.Write("Number: ");
            string inputA = Console.ReadLine();
            int numberA = int.Parse(inputA);

            if (numberA % 2 == 0 || !(numberA > 10))
            {
                Console.Write("even number OR NOT " + numberA + " > 10");
            }
            /*
            else if (numberA < 10)
            {
                Console.Write(numberA + " < 10");
            }*/
            else
            {
                Console.Write("odd number");
            }

            Console.ReadKey();
        }
    }
}
