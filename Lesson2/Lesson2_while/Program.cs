﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2_while
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 2;
            int iteration = 0;

            while (sum % 2 == 0)
            {
                var randomNumber = new Random().Next(10);
                sum += randomNumber;
                iteration++;
                Console.WriteLine("Iteration: " + iteration + " | Sum: " + sum);
            }

            Console.Write("--> Iteration: " + iteration + " | Sum: " + sum );
            Console.ReadKey();
        }
    }
}
