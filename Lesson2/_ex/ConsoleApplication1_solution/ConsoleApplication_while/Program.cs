﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_while
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.Write("Number A: ");
            //string number = Console.ReadLine();
            //int nr = int.Parse(number);

            //while (nr % 2 == 0)
            //{
            //    //string msg1 = "Try again" + Environment.NewLine + "Number A: ";
            //    //string msg2 = string.Format("Try again {0} Number A: {1}", Environment.NewLine);
            //    string msg3 = $"Try again {Environment.NewLine} Number A: ";
            //    Console.WriteLine(msg3);
            //    number = Console.ReadLine();
            //    nr = int.Parse(number);
            //}

            //Console.WriteLine("Done");
            //Console.ReadKey();

            Console.Write("Number A: ");
            string inputStringNumber = Console.ReadLine();
            int nrA = int.Parse(inputStringNumber);

            Console.Write("Number B: ");
            inputStringNumber = Console.ReadLine();
            int nrB = int.Parse(inputStringNumber);

            int sum = nrA + nrB;

            while (sum % 2 == 0)
            {

                string msg3 = $"A: {nrA} {Environment.NewLine}B: {nrB} {Environment.NewLine}Sum is: {sum}. Try again {Environment.NewLine} New Number B: ";
                Console.WriteLine(msg3);

                nrA = nrB;
                inputStringNumber = Console.ReadLine();
                nrB = int.Parse(inputStringNumber);

                sum = nrA + nrB;
            }

            Console.WriteLine("Done");
            Console.ReadKey();
        }
    }
}
