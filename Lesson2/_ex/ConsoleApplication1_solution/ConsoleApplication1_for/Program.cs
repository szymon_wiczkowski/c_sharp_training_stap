﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1_for
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("for: ");

            var names = new[] { "Luck", "Vader", "Yoda", "Han", "Leya" };

            for (int i = 0; i < names.Length; i++)
            {
                Console.WriteLine("Name is: " + names[i]);
            }
            string msg = string.Empty;
            Console.WriteLine("foreach: ");
            foreach (var name in names)
            {
                msg = "Name is: " + name;
                Console.WriteLine(msg);
            }
            Console.WriteLine(msg);
            Console.ReadKey();
        }
    }
}
