﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_firstclass
{
    class Program
    {
        static void Main(string[] args)
        {
            Person luck = new Person();

            Console.Write("What is your first name: ");
            luck.FirstName = Console.ReadLine();

            Console.Write("What is your last name: ");
            luck.LastName = Console.ReadLine();

            luck.SayName();

            Console.ReadKey();
        }
    }
}
