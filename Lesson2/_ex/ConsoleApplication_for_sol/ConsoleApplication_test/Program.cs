﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication_test
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int j = 0; j < 10; j++)
            {
                var n = new Random().Next(0, 100);
                Thread.Sleep(500);
                var n1 = new Random().Next(0, 100);

                int sum = n + n1;
                int iteration = 0;
                string msg = string.Empty;

                while (sum % 2 != 0)
                {
//                    msg = String.Format("iteration: {4}, n: {0}, n1: {1}, sum: {2}, Try again!", n, n1, sum, iteration);
                    msg = $"iteration: {iteration}, n: {n}, n1: {n1}, sum: {sum}, Try again!";

                    Console.WriteLine(msg);
                    n = n1;
                    Thread.Sleep(500);
                    n1 = new Random().Next(0, 100);

                    sum = n + n1;
                    iteration++;
                    //Thread.Sleep(500);
                }

                msg = String.Format("iteration: {3}, n: {0}, n1: {1}, sum: {2}, Done!", n, n1, sum, iteration);
                Console.WriteLine(msg);
                Console.WriteLine("----------------------------------------------------------------");
                Thread.Sleep(500);
            }

            Console.WriteLine("Finish");
            Console.ReadKey();
        }
    }
}
