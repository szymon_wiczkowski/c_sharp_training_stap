﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_for
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("for: ");

            var names = new[] { "Luck", "Vader", "Yoda", "Han", "Leya" };

            for (int i = 0; i < names.Length; i++)
            {
                Console.WriteLine("Name is: " + names[i]);
            }

            Console.WriteLine("foreach: ");

            foreach (string name in names)
            {
                Console.WriteLine("Name is: " + name);
            }

            Console.ReadKey();
        }
    }
}
