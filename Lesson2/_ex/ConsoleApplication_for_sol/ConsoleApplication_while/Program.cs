﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_while
{
    class Program
    {
        static void Main(string[] args)
        {
            var randomNumber = new Random().Next(0, 100);
            int iteration = 0;

            while (randomNumber % 2 != 0)
            {
                Console.WriteLine("iteration: " + iteration + " randomNumber: " + randomNumber + " Try again");
                randomNumber = new Random().Next(0, 100);
                iteration++;
            }

            Console.WriteLine("iteration: " + iteration + " randomNumber: " + randomNumber + " Done");
            Console.ReadKey();
        }
    }
}
