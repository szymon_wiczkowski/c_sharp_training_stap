﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson1_Calc
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine("Calculator");

            Console.Write("A: ");
            string inputA = Console.ReadLine();

            Console.Write("B: ");
            string inputB = Console.ReadLine();

            int sum = int.Parse(inputA) + int.Parse(inputB);

            Console.Write("Sum: " + sum);

            Console.ReadKey();
        }
    }
}
