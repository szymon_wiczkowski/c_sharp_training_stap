﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3_first_class
{
    class Person
    {
        private string FirstName { get; set; }

        string _lastName;
        private string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = "von " + value;
            }
        }

        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }

        private string PESEL { get; set; }

        private DateTime DateOfBirth { get; set; }

        public Person(string firstName, string lastName, string pesel)
        {
            if (IsInDb(pesel))
                throw new Exception($"Psesel {pesel} exists in DB");

            PESEL = pesel + "1";
            FirstName = firstName;
            LastName = lastName;
        }

        public Person(string firstName, string lastName, string pesel, DateTime dateOfBirth)
            : this(firstName, lastName, pesel)
        {
            DateOfBirth = dateOfBirth;
        }

        public void SayMyName()
        {
            var message = string.Format("My name is {0}", FullName);
            Console.WriteLine(message);
        }

        private bool IsInDb(string PESEL)
        {
            return false;
        }
    }
}
