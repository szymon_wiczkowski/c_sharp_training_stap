﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3_first_class
{
    class Program
    {
        static void Main(string[] args)
        {
            Person luck = new Person("Luck", "Skywalker", "99999999999");
            //luck.FirstName = "Luck";
            //luck.LastName = "Skywalker";
            //luck.DateOfBirth = new DateTime(1951, 9, 25);
            //luck.PESEL = "99999999999";
            luck.SayMyName();

            Person vader = new Person("Lord", "Vader", "88888888888");
            //vader.FirstName = "Lord";
            //vader.LastName = "Vader";
            //vader.DateOfBirth = new DateTime(1951, 9, 25);
            //vader.PESEL = "99999999999";
            vader.SayMyName();

            Person han = new Person("Han", "Solo", "77777777777", new DateTime(1951, 9, 25));
            han.SayMyName();

            /*
            Person luck = new Person("Luck", "Skywalker");
            luck.DateOfBirth = new DateTime(1951, 9, 25);
            luck.SayMyName();
            */

            /*
            Person luck = new Person("Luck", "Skywalker", new DateTime(1951, 9, 25));
            luck.PESEL = "99999999999";
            luck.SayMyName();
            */

            /*
            string[] tablica = new string[2];
            tablica[0] = "Han";
            tablica[1] = "Luck";

            int[] tablicaIntow = new int[1000];

            Random ran = new Random();
            for (int i = 0; i < tablicaIntow.Length; i++)
            {
                tablicaIntow[i] = ran.Next();
            }

            for (int i = 0; i < tablicaIntow.Length; i)
            {
                Console.WriteLine(tablicaIntow[i]);
            }

            foreach (var element in tablicaIntow)
            {
                Console.WriteLine(element);
            }
            */
            Console.ReadKey();
        }
    }
}
