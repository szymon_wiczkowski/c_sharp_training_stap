﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonCar
{
    class Person
    {
        private string FirstName { get; set; }

        private string LastName { get; set; }

        public bool IsDriver { get; set; }

        public string FullName
        {
            get
            {
                return $"{LastName} {FirstName}";
            }
        }

        public string Description
        {
            get
            {
                return $"{LastName} {FirstName} - is driver: {IsDriver}";
            }
        }

        public Person(string firstName, string lastName, bool isDriver)
        {
            FirstName = firstName;
            LastName = lastName;
            IsDriver = isDriver;
        }
    }
}
