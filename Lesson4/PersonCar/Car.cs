﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonCar
{
    class Car
    {
        public Person[] Passengers { get; private set; }

        public int MaxPassengersCount { get; set; }

        public Car(int maxPassengersCount)
        {
            MaxPassengersCount = maxPassengersCount;
            Passengers = new Person[MaxPassengersCount];
        }

        public void AddPassenger(Person person)
        {
            for (int i = 0; i < Passengers.Length; i++) //iterujemy po wszystkich miejscach
            {
                //Console.WriteLine($"Sprawdzamy miejsce: {i}");
                //Console.WriteLine($"Osoba wsiadajaca: {person.Description}");

                if (Passengers[i] == null) //sprawdzamy czy miejsce jest wolne
                {
                    if (i == 0 && person.IsDriver)  //miejsce kierowcy i jest kierowca
                    {
                        Passengers[i] = person;
                        break;
                    }
                    else if (i == 0 && !person.IsDriver) //miejsce kierowcy i nie jest kierowca
                    {
                        continue;
                    }
                    else
                    {
                        Passengers[i] = person; //miejsce nie kierowcy
                        break;
                    }
                }
            }
        }

        public void RemovePassenger(int passangerIndex)
        {
            Passengers[passangerIndex] = null;

            if (passangerIndex != 0)
            {
                return;
            }

            for (int i = 1; i < Passengers.Length; i++)
            {
                if (Passengers[i] != null && Passengers[i].IsDriver)
                {
                    Passengers[0] = Passengers[i];
                    Passengers[i] = null;
                    break;
                }
            }
        }

        public void ShowAllPassengers()
        {
            Console.WriteLine("Passengers state");

            for (int i = 0; i < Passengers.Length; i++)
            {

                if (Passengers[i] == null)
                {
                    Console.WriteLine($"index: {i}, empty place");
                }
                else
                {
                    Console.WriteLine($"index: {i}, person: {Passengers[i].Description}");
                }
            }

            Console.WriteLine("--------------------------------");
        }

        public void StartEngine()
        {
            if (Passengers[0] == null)
            {
                Console.WriteLine("You need driver");
                return;
            }

            Console.WriteLine("Brum Brum");
        }
    }
}
