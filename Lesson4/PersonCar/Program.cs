﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonCar
{
    class Program
    {
        static void Main(string[] args)
        {
            Person c3po = new Person("C3PO", "C3PO", false);
            Person luck = new Person("Luck", "Skywalker", true);
            Person jarjar = new Person("Jar", "Jar", false);
            Person vader = new Person("Vader", "Darth", true);

            Car auto = new Car(7);  //tworzymy 5osobowy samochod

            auto.ShowAllPassengers();

            auto.AddPassenger(c3po);    //wsiada c3po
            auto.ShowAllPassengers();
            auto.StartEngine();         //samochod rusza

            auto.AddPassenger(luck);    //wsiada luck
            auto.ShowAllPassengers();
            auto.StartEngine();

            auto.AddPassenger(jarjar);  //wsiada jarjar
            auto.ShowAllPassengers();

            auto.AddPassenger(vader);   //wsiada vader
            auto.ShowAllPassengers();

            auto.RemovePassenger(2);    //wysiada osoba z miejsca 2
            auto.ShowAllPassengers();
            auto.StartEngine();

            auto.RemovePassenger(0);    //wysiada kierowca
            auto.ShowAllPassengers();
            auto.StartEngine();

            auto.RemovePassenger(0);    //wysiada kierowca
            auto.ShowAllPassengers();
            auto.StartEngine();

            Console.ReadLine();
        }
    }
}
