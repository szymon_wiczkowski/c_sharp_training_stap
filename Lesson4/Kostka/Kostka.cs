﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kostka
{
    class Kostka
    {
        private int[] Scianki { get; set; }

        public int IloscScianek
        {
            get
            {
                return Scianki.Length;
            }
        }

        Random _losowacz;

        public Kostka() : this(6)
        {

        }

        public Kostka(int iloscScianek, int sumaOczek) : this(iloscScianek)
        {

        }

        public Kostka(int iloscScianek)
        {
            _losowacz = new Random();

            Scianki = new int[iloscScianek];

            for (int i = 0; i < iloscScianek; i++)
            {
                Scianki[i] = i + 1;
            }
        }

        public int? OdczytajWartoscZeScianki(int indexScianki)
        {
            if (indexScianki > Scianki.Length - 1)
                return null;

            return Scianki[indexScianki];
        }

        public int Rzuc()
        {
            int wylosowanyIndex = _losowacz.Next(0, Scianki.Length);
            int? wylosowanaWartosc = OdczytajWartoscZeScianki(wylosowanyIndex);
            return (int)wylosowanaWartosc;
        }
    }
}
