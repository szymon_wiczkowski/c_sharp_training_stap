﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kostka
{
    class Program
    {
        static void Main(string[] args)
        {
            Kostka koscKlasyk = new Kostka(6);
            Kostka kosc = new Kostka(6);
            Kostka koscDuza = new Kostka(120);

            Console.WriteLine($"kosc klasyk: {koscKlasyk.Rzuc()}");
            Console.WriteLine($"kosc klasyk: {koscKlasyk.Rzuc()}");
            Console.WriteLine($"kosc klasyk: {koscKlasyk.Rzuc()}");
            Console.WriteLine($"kosc klasyk: {koscKlasyk.Rzuc()}");
            Console.WriteLine($"kosc klasyk: {koscKlasyk.Rzuc()}");

            Console.WriteLine($"kosc: {kosc.Rzuc()}");
            Console.WriteLine($"kosc: {kosc.Rzuc()}");
            Console.WriteLine($"kosc: {kosc.Rzuc()}");
            Console.WriteLine($"kosc: {kosc.Rzuc()}");
            Console.WriteLine($"kosc: {kosc.Rzuc()}");

            Console.WriteLine($"kosc duza: {koscDuza.Rzuc()}");
            Console.WriteLine($"kosc duza: {koscDuza.Rzuc()}");
            Console.WriteLine($"kosc duza: {koscDuza.Rzuc()}");
            Console.WriteLine($"kosc duza: {koscDuza.Rzuc()}");
            Console.WriteLine($"kosc duza: {koscDuza.Rzuc()}");
            
            Console.ReadKey();
        }
    }
}
