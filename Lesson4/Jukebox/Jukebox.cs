﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jukebox
{
    class Jukebox
    {
        private List<ITrack> Songs { get; set; }

        public int? PlayingTrackNumber { get; set; }

        public Jukebox()
        {
            Songs = new List<ITrack>();
        }

        public void AddTrack(ITrack newTrack)
        {
            Songs.Add(newTrack);
        }

        public void ShowaAllSongs()
        {
            for (int i = 0; i < Songs.Count; i++)
            {
                Songs[i].ShowInfo();
            }
        }

        public void PlaySong(int truckNumber)
        {
            PlayingTrackNumber = truckNumber;

            int trackIndex = (int)PlayingTrackNumber;
            Songs[trackIndex].ShowInfo();

            ShowPlayingSong();
        }

        public void ShowPlayingSong()
        {
            if (!PlayingTrackNumber.HasValue)
            {
                Console.WriteLine("no song is playing");
                return;
            }

            Songs[(int)PlayingTrackNumber].ShowInfo();
        }    
    }
}
