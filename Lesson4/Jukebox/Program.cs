﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jukebox
{
    class Program
    {
        static void Main(string[] args)
        {
            Jukebox radio = new Jukebox();

            Mp3 mp3 = new Mp3("nirvana", "smells like teen spirit", 320, 7);
            Mp3 track1 = new Mp3("Eminem", "Stan", 360, 5);
            VinylTrack track2 = new VinylTrack("Pink Floyd", "Wall", 560);
            CasseteTrack track3 = new CasseteTrack("Weekend", "Ona tanczy dla mnie", 260, 1);
            LiveStreamVideo video = new LiveStreamVideo("Canal+ Sport", "FCB vs RM");

            radio.AddTrack(track1);
            radio.AddTrack(track2);
            radio.AddTrack(track3);
            radio.AddTrack(mp3);
            radio.AddTrack(video);

            radio.ShowaAllSongs();

            Console.ReadLine();
        }
    }
}
