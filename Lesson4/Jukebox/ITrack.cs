﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jukebox
{
    interface ITrack
    {
        string Author { get; set; }

        string Title { get; set; }

        void ShowInfo();
    }
}
