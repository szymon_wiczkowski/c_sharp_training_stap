﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jukebox
{
    class VinylTrack : Track
    {
        public VinylTrack(string author, string title, int time) : base(author, title, time)
        {
        }
    }
}
