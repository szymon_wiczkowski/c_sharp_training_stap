﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jukebox
{
    class CasseteTrack : Track
    {
        public int SongSide { get; set; }

        public CasseteTrack(string author, string title, int time, int songSide)
            : base(author, title, time)
        {
            SongSide = songSide;
        }
    }
}
