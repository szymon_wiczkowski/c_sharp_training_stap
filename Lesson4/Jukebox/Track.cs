﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jukebox
{
    class Track : ITrack
    {
        public string Author { get; set; }

        public int Time { get; set; }

        public string Title { get; set; }

        public Track(string author, string title, int time)
        {
            Author = author;
            Title = title;
            Time = time;
        }

        public virtual void ShowInfo()
        {
            string info = string.Format("author: {0}  title: {1} | time: {2}", Author, Title, Time);
            Console.WriteLine(info);
        }
    }
}
