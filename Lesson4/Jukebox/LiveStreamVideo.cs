﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jukebox
{
    class LiveStreamVideo : ITrack
    {
        public string Author { get; set; }

        public string Title { get; set; }

        public LiveStreamVideo(string author, string title)
        {
            Author = author;
            Title = title;
        }

        public void ShowInfo()
        {
            string info = string.Format("author: {0}  title: {1}", Author, Title);
            Console.WriteLine(info);
        }
    }
}
