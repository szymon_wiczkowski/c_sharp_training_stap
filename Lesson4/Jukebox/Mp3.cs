﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jukebox
{
    class Mp3 : Track
    {
        public int Size { get; set; }

        public Mp3(string author, string title, int time, int size)
            : base(author, title, time)
        {
            Size = size;
        }

        public override void ShowInfo()
        {
            string info = string.Format("author: {0}  title: {1} | time: {2} | size: {3}", Author, Title, Time, Size);
            Console.WriteLine(info);
        }
    }
}
